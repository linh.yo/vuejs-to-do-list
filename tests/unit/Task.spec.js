import { mount } from '@vue/test-utils'
import Task from '@/components/Task.vue'

describe('Task Component unit tests', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Task, {
      propsData: {
        task: {
          name: "Task 1",
          completed: false
        }
      }
    });
    expect(wrapper.vm).toBeTruthy();
  });

  test('renders the task name', () => {
    const taskName = "Task 1";
    const wrapper = mount(Task, {
      propsData: {
        task: {
          name: taskName,
          completed: false
        }
      }
    });
    expect(wrapper.html()).toContain(taskName);
  });

  // test('calls deleteTask when the delete button is clicked', () => {
  //   const wrapper = mount(Task, {
  //     propsData: {
  //       task: {
  //         name: "Task 2",
  //         completed: false
  //       }
  //     }
  //   });
  //   const deleteTask = jest.fn();
  //
  //   wrapper.setMethods({
  //     deleteTask: deleteTask
  //   });
  //
  //   wrapper.find('a').trigger('click');
  //
  //   expect(deleteTask).toHaveBeenCalled();
  // });

  // test('calls markComplete function when the checkbox is clicked', () => {
  //   const wrapper = mount(Task, {
  //     propsData: {
  //       task: {
  //         name: "Task 2",
  //         completed: false
  //       }
  //     }
  //   });
  //   const onSelect = jest.fn();
  //
  //   wrapper.setMethods({
  //     onSelect: onSelect
  //   });
  //
  //   wrapper.find('a').trigger('click');
  //
  //   expect(onSelect).toHaveBeenCalled();
  // });
})
